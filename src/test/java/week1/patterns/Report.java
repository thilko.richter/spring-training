package week1.patterns;

public class Report {

    boolean isPrivate = false;

    private Report(boolean isPrivate) {
        this.isPrivate = isPrivate;
    }

    public static Report createPrivate() {
        return new Report(true);
    }

    public void show() {

    }
}
