package week1.patterns;

import org.junit.jupiter.api.Test;

public class PatternTest {

    @Test
    public void factoryMethod(){
        Report firstReport = Report.createPrivate();
        firstReport.show();
    }

}
