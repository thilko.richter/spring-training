package week1.streams;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class StreamsTest {

    @Test
    public void forLoop() {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7);

        for (int i = 0; i < numbers.size(); i++) {
            System.out.println(numbers.get(i));
        }
    }

    @Test
    public void forEachAsStream(){
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7);

        numbers.stream().forEach((integer) -> {
            System.out.println("I" + integer);
        });
    }

    @Test
    public void mapAsStream() {
        List<Integer> numbers = Arrays.asList(1, 2, 3, 4, 5, 6, 7);
        numbers.stream()
                .map(integer -> integer * 5)
                .map(integer -> integer * 10)
                .forEach(integer -> System.out.println("I" + integer));
    }
}

