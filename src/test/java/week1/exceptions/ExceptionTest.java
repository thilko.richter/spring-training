package week1.exceptions;

import org.junit.jupiter.api.Test;

public class ExceptionTest {

    @Test
    public void uncheckedException(){
        // unchecked do not have to be caught.
        //divideWithRuntimeException(1, 0);
    }

    @Test
    public void checkedException(){
        // Checked exception must be handled, it is forced by the compiler.
        try {
            divideWithCheckedException(1, 0);
        } catch (DivisionByZeroException e) {
            System.out.println(e);
        }
    }

    private void divideWithCheckedException(int i, int i1) throws DivisionByZeroException {
        if(i1 <= 0){
            throw new DivisionByZeroException();
        }
        System.out.println(i / i1);
    }

    private void divideWithRuntimeException(int i, int i1) {
        if(i1 < 0){
            throw new IllegalArgumentException();
        }
        System.out.println(i / i1);
    }

    private class DivisionByZeroException extends Exception { }
}
