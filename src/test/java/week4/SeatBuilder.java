package week4;

import com.datev.springtraining.seat.Seat;

public class SeatBuilder {

    private int row, column;
    private boolean available;

    public SeatBuilder(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public static SeatBuilder aSeat() {
        return new SeatBuilder(5, 5);
    }

    public SeatBuilder withRow(int row) {
        this.row = row;
        return this;
    }

    public SeatBuilder withColumn(int column) {
        this.column = column;
        return this;
    }

    public Seat build() {
        Seat seat = new Seat(row, column);
        if (!available) {
            seat.markAsOccupied();
        }
        return seat;
    }

    public SeatBuilder asAvailable() {
        this.available = true;
        return this;
    }

    public SeatBuilder asOccupied() {
        this.available = false;
        return this;
    }
}
