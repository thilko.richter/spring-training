package week4.integration;

import com.datev.springtraining.App;
import com.datev.springtraining.seat.Seat;
import com.datev.springtraining.ticket.ReturnTicketRequest;
import com.datev.springtraining.ticket.Ticket;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import week4.TestApi;
import week4.TicketBuilder;

import java.io.UnsupportedEncodingException;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static week4.PurchaseRequestBuilder.aPurchaseRequest;
import static week4.SeatBuilder.aSeat;

@SpringBootTest(classes = {App.class})
@AutoConfigureMockMvc
@Sql({"/data.sql"})
public class TicketControllerTest {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    MockMvc mockMvc;

    private TestApi testApi;

    @BeforeEach
    public void beforeEach() {
        testApi = new TestApi(objectMapper, mockMvc);
    }

    @Test
    public void purchase_purchaseIsValid_seatIsNotAvailable() throws Exception {
        testApi.purchase(aPurchaseRequest().build())
                .andExpect(status().isOk());

        testApi.getAllSeats()
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.available_seats", hasSize(80)));
    }

    @Test
    public void purchase_purchaseHasRowGt10_throwsHttp400() throws Exception {
        int rowNotInRange = 11;
        testApi.purchase(aPurchaseRequest().withRow(rowNotInRange).build())
                .andExpect(status().is4xxClientError());
    }

    @Test
    public void returnTicket_validReturnOfAPurchasedTicket_valid() throws Exception {
        ResultActions purchase = testApi.purchase(aPurchaseRequest().withColumn(5).withRow(6).build());
        Ticket ticket = asTicket(purchase);

        ResultActions resultActions = testApi.returnTicket(ticket);
        resultActions.andExpect(status().isOk())
                .andExpect(jsonPath("$.returned_ticket.row").value(6))
                .andExpect(jsonPath("$.returned_ticket.column").value(5))
                .andExpect(jsonPath("$.returned_ticket.price").value(8));
    }

    @Test
    public void returnTicket_alreadyReturnedTicket_returnsHttp400() throws Exception {
        ResultActions purchase = testApi.purchase(aPurchaseRequest().withColumn(5).withRow(6).build());
        Ticket ticket = asTicket(purchase);

        testApi.returnTicket(ticket);
        ResultActions resultActions = testApi.returnTicket(ticket);

        resultActions.andExpect(status().is4xxClientError());
    }

    @Test
    public void returnTicket_invalidToken_returnsHttp400() throws Exception {
        ResultActions response = testApi.returnTicket(TicketBuilder.aTicket().withToken("invalid-token").build());
        response.andExpect(status().is4xxClientError());
    }

    private Ticket asTicket(ResultActions resultActions) throws JsonProcessingException, UnsupportedEncodingException {
        return objectMapper.readValue(resultActions.andReturn().getResponse().getContentAsString(), Ticket.class);
    }

}
