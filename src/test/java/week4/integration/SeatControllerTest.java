package week4.integration;

import com.datev.springtraining.App;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import week4.TestApi;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = {App.class})
@AutoConfigureMockMvc
@Sql({"/data.sql"})
public class SeatControllerTest {

        @Autowired
        ObjectMapper objectMapper;

        @Autowired
        MockMvc mockMvc;

        private TestApi testApi;

        @BeforeEach
        public void beforeEach() {
            testApi = new TestApi(objectMapper, mockMvc);
        }

        @Test
        public void getSeats_allSeatsAreReturned() throws Exception {
            testApi.getAllSeats()
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.total_columns").value(9))
                    .andExpect(jsonPath("$.total_rows").value(9))
                    .andExpect(jsonPath("$.available_seats", hasSize(81)));
        }

}
