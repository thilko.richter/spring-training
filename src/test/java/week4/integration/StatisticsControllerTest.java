package week4.integration;

import com.datev.springtraining.App;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;
import week4.TestApi;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = {App.class})
@AutoConfigureMockMvc
@Sql({"/data.sql"})
public class StatisticsControllerTest {


    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    MockMvc mockMvc;

    private TestApi testApi;

    @BeforeEach
    public void beforeEach() {
        testApi = new TestApi(objectMapper, mockMvc);
    }

    @Test
    public void getStatistics_noTicketSold_allSeatsAvailable() throws Exception {
        testApi.getStatistics("super_secret")
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.current_income").value(0))
                .andExpect(jsonPath("$.number_of_available_seats").value(81))
                .andExpect(jsonPath("$.number_of_purchased_tickets").value(0));
    }

    @Test
    public void getStatistics_wrongPasswordProvided_returnsHttp401() throws Exception {
        testApi.getStatistics("wrong-password")
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void getStatistics_noPasswordProvided_returnsHttp401() throws Exception {
        testApi.getStatistics()
                .andExpect(status().isUnauthorized());
    }
}
