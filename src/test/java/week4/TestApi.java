package week4;

import com.datev.springtraining.ticket.PurchaseRequest;
import com.datev.springtraining.ticket.ReturnTicketRequest;
import com.datev.springtraining.ticket.Ticket;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class TestApi {

    ObjectMapper objectMapper;

    MockMvc mockMvc;

    public TestApi(ObjectMapper objectMapper, MockMvc mockMvc) {
        this.objectMapper = objectMapper;
        this.mockMvc = mockMvc;
    }

    public ResultActions getAllSeats() throws Exception {
        return mockMvc.perform(get("/seats"));
    }

    public ResultActions purchase(PurchaseRequest aPurchase) throws Exception {
        return mockMvc.perform(post("/purchase")
                .content(objectMapper.writeValueAsString(aPurchase))
                .contentType(MediaType.APPLICATION_JSON));

    }

    public ResultActions returnTicket(Ticket ticket) throws Exception {
        return mockMvc.perform(post("/return")
                .content(objectMapper.writeValueAsString(new ReturnTicketRequest(ticket.getToken())))
                .contentType(MediaType.APPLICATION_JSON));
    }

    public ResultActions getStatistics(String password) throws Exception {
        return mockMvc.perform(get("/stats?password={password}", password));
    }

    public ResultActions getStatistics() throws Exception {
        return mockMvc.perform(get("/stats"));
    }
}
