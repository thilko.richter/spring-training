# Expense Report Legacy Kata
Die Expense Report Kata ist eine Legacy Code Kata. Ziel der Aufgabe ist es, ungetestenen Code zu testen und danach durch verschiedene Refactorings zu verbessern.

Das Original findet sich hier: [Expense Report](https://github.com/christianhujer/expensereport)

## Ablauf

1. Schreibe "Golden Master Tests" um das aktuelle Verhalten zu spezifizieren. Verändere vorher nicht den Produktivcode! Teste folgende Szenarien:
   1. Report wird ohne Ausgaben ausgedruckt.
   2. Report wird mit einer Ausgabe ausgedruckt. Schreibe jeweils einen Test für die Typen "DINNER", "BREAKFAST", "CAR_RENTAL".
   3. Report wird 2 Ausgaben ausgedruckt. Schreibe jeweils einen Test für "DINNER", "BREAKFAST".
   4. Report wird 2 Ausgaben ausgedruckt, deren Betrag über den jeweiligen Ausgabenlimit liegt. Schreibe jeweils einen Test für "DINNER", "BREAKFAST".

2. Mit einer ausreichenden Testabdeckung kannst Du nun das Design des Systems verbessern, ohne bestehende Funktionalität zu verändern. Denke daran,
    kleinschrittig vorzugehen und nach jedem Refactoring die Tests neu auszuführen. Anbei ein paar Beispiele:

   1. Die Methode `printReport` verletzt das "Single Responsibilty" Prinzip. Extrahiere neue Methoden aus der Methode `printReport` und 
   gebe ihnen einen entsprechenden Namen.   
    
   2. Extrahiere die Berechnung der Summen (für Essensausgaben und die Gesamtsumme) in eine neue Klasse und schreibe dafür neue Unit Tests. 
   
   3. Führe eine neue Schnittstelle `ReportPrinter` ein, die im `ExpenseReport` benutzt wird, damit Du die Klasse `ExpenseReport` ohne
        externe Testwerkzeuge testen kannst. Zunächst hat der `ReportPrinter` lediglich eine Implementierung die nach `System.out` schreibt,
        benutze danach eine andere Implementierung, um die Tests ohne `CapturedOutput` schreiben zu können.

   4. Zukünftig sollen auch HTML Reports unterstützt werden. Dafür ist es notwendig, die Darstellung eines Reports(txt, html) von der Logik
      zu trennen. Führe eine neue Klasse ein (z.B. `PrintableReport`) die alle berechneten Werte und Ausgaben beinhaltet.
   
3. Füge ein lang ersehntes Feature hinzu: das Programm soll eine neue Ausgabe mit dem Typ `Lunch` und einem Limit von `2000` unterstützen.