package com.datev.springtraining.ticket;

public class ReturnTicketRequest {

    private String token;

    private ReturnTicketRequest(){}

    public ReturnTicketRequest(String token){
        this.token = token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
