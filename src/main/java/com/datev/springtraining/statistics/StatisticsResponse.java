package com.datev.springtraining.statistics;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class StatisticsResponse {

    private long currentIncome, numberOfAvailableSeats, numberOfPurchasedTickets;

    public static StatisticsResponse from(Statistics statistics) {
        var response = new StatisticsResponse();
        response.currentIncome = statistics.getCurrentIncome();
        response.numberOfAvailableSeats = statistics.getAvailableSeats();
        response.numberOfPurchasedTickets = statistics.getNumberOfPurchasedTickets();

        return response;
    }
}
