package com.datev.springtraining.seat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SeatController {

    @Autowired
    private SeatService seatService;

    @GetMapping("/seats")
    public AllAvailableSeatsResponse getSeats() {
        var response = new AllAvailableSeatsResponse();
        response.setAvailableSeats(AvailableSeatResponse.from(seatService.getAvailableSeats()));
        response.setTotalColumns(9);
        response.setTotalRows(9);

        return response;
    }

}
