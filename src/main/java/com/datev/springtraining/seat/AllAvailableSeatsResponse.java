package com.datev.springtraining.seat;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import java.util.List;

@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class AllAvailableSeatsResponse {

    private int totalRows, totalColumns;
    private List<AvailableSeatResponse> availableSeats;

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public void setTotalColumns(int totalColumns) {
        this.totalColumns = totalColumns;
    }

    public void setAvailableSeats(List<AvailableSeatResponse> availableSeats) {
        this.availableSeats = availableSeats;
    }
}
