package com.datev.springtraining.seat;

import java.util.List;
import java.util.stream.Collectors;

public class AvailableSeatResponse {

    private int column;
    private int row;

    private AvailableSeatResponse(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public static List<AvailableSeatResponse> from(List<Seat> availableSeats) {
        return availableSeats.stream().map(AvailableSeatResponse::mapFrom).collect(Collectors.toList());
    }

    private static AvailableSeatResponse mapFrom(Seat availableSeat) {
        return new AvailableSeatResponse(availableSeat.getRow(), availableSeat.getSeatColumn());
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }
}
