package com.datev.springtraining.seat;

import com.datev.springtraining.ticket.PurchaseRequest;

import javax.persistence.*;

@Entity
public class Seat {

    @Id
    @GeneratedValue
    private int id;

    @Column(name = "seat_row")
    private int seatRow;

    @Column(name = "seat_column")
    private int seatColumn;

    @Column
    private boolean available;

    @SuppressWarnings("unused")
    private Seat(){}

    public Seat(int row, int seatColumn) {
        this.seatRow = row;
        this.seatColumn = seatColumn;
        this.available = true;
    }

    public int getRow() {
        return seatRow;
    }

    public int getSeatColumn() {
        return seatColumn;
    }

    public boolean matchesPurchase(PurchaseRequest request) {
        return getRow() == request.getRow() && getSeatColumn() == request.getColumn();
    }

    public void markAsOccupied() {
        this.available = false;
    }

    public boolean isAvailable() {
        return this.available;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Seat seat = (Seat) o;

        if (this.seatRow != seat.seatRow) return false;
        if (seatColumn != seat.seatColumn) return false;
        return available == seat.available;
    }

    @Override
    public int hashCode() {
        int result = seatRow;
        result = 31 * result + seatColumn;
        result = 31 * result + (available ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Seat{" +
                "id=" + id +
                ", row=" + seatRow +
                ", column=" + seatColumn +
                ", available=" + available +
                '}';
    }

    public void markAsAvailable() {
        this.available = true;
    }

    public void setId(int id) {
        this.id = id;
    }
}
