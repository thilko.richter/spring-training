package com.datev.springtraining.seat;

import com.datev.springtraining.statistics.PasswordWrong;
import com.datev.springtraining.ticket.AlreadyBooked;
import com.datev.springtraining.ticket.WrongToken;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class Exceptions {

    @ExceptionHandler({AlreadyBooked.class})
    public ResponseEntity<ErrorResponse> alreadyBooked() {
        var r = new ErrorResponse();
        r.error = "The ticket has been already purchased!";

        return ResponseEntity.badRequest().body(r);
    }

    @ExceptionHandler({NumberInvalid.class})
    public ResponseEntity<ErrorResponse> numberInvalid() {
        var r = new ErrorResponse();
        r.error = "The number of a row or a column is out of bounds!";

        return ResponseEntity.badRequest().body(r);
    }

    @ExceptionHandler({WrongToken.class})
    public ResponseEntity<ErrorResponse> wrongToken() {
        var r = new ErrorResponse();
        r.error = "Wrong token!";

        return ResponseEntity.badRequest().body(r);
    }

    @ExceptionHandler({PasswordWrong.class})
    public ResponseEntity<ErrorResponse> passwordWrong() {
        var r = new ErrorResponse();
        r.error = "The password is wrong!";

        return ResponseEntity.status(401).body(r);
    }
}
