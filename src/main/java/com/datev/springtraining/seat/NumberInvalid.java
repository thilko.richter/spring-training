package com.datev.springtraining.seat;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class NumberInvalid extends RuntimeException {

    private final String message;

    public NumberInvalid(String s) {
        message = s;
    }
}
