# DATEV spring training
Herzlich Willkommen!. Hier findest du alle Aufgaben und Informationen zum Spring Training im Rahmen des Progamms "Becoming a software engineer".

Das Repo ist als Arbeitsplatz unserer gemeinsamen Übungen gedacht und existiert parallel zu den Übungen in Hyperskill. 

## Ablauf

### 1. Woche
In der ersten Woche liegt der Schwerpunkt auf TDD. Hier findest Du zwei Aufgaben, die "Roman Numerals" und die "String Calculator" Kata. Beide Aufgaben eignen sich sehr gut für das Erlernen von testgetriebener Entwicklung.

### 2. Woche
Das Arbeit mit Legacy Code steht in der zweiten Wochen im Vordergrund.

### 3. Woche
Wir machen eine Spaziergang durch Spring und setzen Schritt für Schritt eine eigene Spring Anwendung auf.

### 4. Woche
Der Schwerpunkt liegt auf Spring Data JPA.

### 5. Woche
Wir arbeiten mit JPA Annotations und betrachten das Mapping des Objektmodells in die Datenbank näher.

### 6. Woche
Gemeinsam reflektieren wir das Training.

## Nützliche Links
[Spring Boot Doc](https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/)
[Spring Data JPA Doc](https://docs.spring.io/spring-data/jpa/docs/current/reference/html/#reference)
[DB Console](http://localhost:8080/h2-console)
[ConceptBoard](https://datev.conceptboard.com/board/ps1m-uhx3-hye6-xdig-1c8y)
[Hyperskill Projekt](https://hyperskill.org/projects/189)

## Spring Service starten
Der Service kann entweder über IntelliJ mit der RunConfiguration gestartet werden, oder über:

    ./mvnw spring-boot:run

## Tests ausführen
Die Anwendung ist mit Unit-, und Integrationstests getestet. Die Tests können über Intellij gestartet werden, oder über

    ./mvnw test